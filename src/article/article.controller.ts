import { Controller, Post, Body, Param, Delete } from '@nestjs/common';
import { ArticleService } from './article.service';
import { Cron, CronExpression } from '@nestjs/schedule';
import { GetArticleDto } from './dto/get-article.dto';
import { ApiParam, ApiTags } from '@nestjs/swagger';

@ApiTags('Article')
@Controller('article')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  // Cron job to run every hour to fetch and save articles by the given API url specified in the keys.ts file
  @Cron(CronExpression.EVERY_HOUR)
  // @Cron(CronExpression.EVERY_10_SECONDS)
  async fetchAndSaveArticles(): Promise<void> {
    this.articleService.fetchAndSaveArticles();
  }

  // Post route to get the articles previously fetched by the cron job, can be filtered by the GetArticleDto
  @Post('get')
  getArticles(@Body() getArticleDto: GetArticleDto) {
    return this.articleService.getArticles(getArticleDto);
  }

  // Delete route to delete the persisted article data in the mongodb, it needs the ObjectId to do so.
  @ApiParam({ name: 'id' })
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.articleService.deleteArticle(id);
  }
}

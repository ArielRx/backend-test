import { Test } from '@nestjs/testing';
import { ArticleService } from './article.service';
import keys from '../../config/keys';
import { Article, ArticleDocument } from '../../src/schemas/article.schema';
import { getModelToken } from '@nestjs/mongoose';
import axios from 'axios';
import { CreateArticleDto } from './dto/create-article.dto';
import { Model } from 'mongoose';
import { GetArticleDto } from './dto/get-article.dto';

jest.mock('axios');

describe('ArticleService', () => {
  let articleService: ArticleService;
  let articleModel: Model<ArticleDocument>;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      providers: [
        ArticleService,
        {
          provide: getModelToken(Article.name),
          useValue: {
            create: jest.fn(),
            find: jest.fn(),
            deleteOne: jest.fn(),
          },
        },
      ],
    }).compile();

    articleService = moduleRef.get<ArticleService>(ArticleService);
    articleModel = moduleRef.get<Model<ArticleDocument>>(
      getModelToken(Article.name)
    );
  });

  describe('fetchAndSaveArticles', () => {
    it('should fetch articles and save them to database', async () => {
      const createArticleDto: CreateArticleDto = {
        author: 'John Doe',
        title: 'Test article',
        url: 'http://example.com/test',
        objectID: '1',
        createdAt: new Date('2022-04-22T12:00:00Z'),
        tags: ['test'],
      };

      const response = {
        data: {
          hits: [
            {
              ...createArticleDto,
              created_at: '2022-04-22T12:00:00.000Z',
              _tags: ['test'],
            },
          ],
        },
      };

      (axios.get as jest.Mock).mockResolvedValueOnce(response);

      await articleService.fetchAndSaveArticles();

      expect(axios.get).toHaveBeenCalledWith(keys.apiURL);
      expect(articleModel.create).toHaveBeenCalledWith(
        expect.arrayContaining([
          {
            author: 'John Doe',
            createdAt: expect.any(Date),
            objectID: '1',
            tags: ['test'],
            title: 'Test article',
            url: 'http://example.com/test',
          },
        ])
      );
    });
  });

  describe('getArticles', () => {
    it('should return an array of articles', async () => {
      const getArticleDto: GetArticleDto = {
        author: 'John Doe',
        tags: ['test'],
        title: 'Test Article',
      };

      const articles: Article[] = [
        {
          author: 'John Doe',
          title: 'Test Article',
          url: 'http://example.com/test',
          createdAt: new Date('2022-04-22T12:00:00Z'),
          tags: ['test'],
        },
      ];

      (articleModel.find as jest.Mock).mockReturnValueOnce({
        sort: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        limit: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        exec: jest.fn().mockResolvedValueOnce(articles),
      });

      const result = await articleService.getArticles(getArticleDto);
      expect(result).toEqual(articles);
    });

    it('should return an empty array if there are no matching articles', async () => {
      const getArticleDto: GetArticleDto = {
        author: 'Jane Doe',
        tags: ['test'],
        title: 'Test Article',
      };

      (articleModel.find as jest.Mock).mockReturnValueOnce({
        sort: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        limit: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        exec: jest.fn().mockResolvedValueOnce([]),
      });

      const result = await articleService.getArticles(getArticleDto);
      expect(result).toEqual([]);
    });

    it('should handle errors', async () => {
      const getArticleDto: GetArticleDto = {
        author: 'John Doe',
        tags: ['test'],
        title: 'Test Article',
      };

      const error = new Error('Database error');

      (articleModel.find as jest.Mock).mockReturnValueOnce({
        sort: jest.fn().mockReturnThis(),
        skip: jest.fn().mockReturnThis(),
        limit: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        exec: jest.fn().mockRejectedValueOnce(error),
      });

      await expect(articleService.getArticles(getArticleDto)).rejects.toThrow(
        error
      );
    });
  });

  describe('deleteArticle', () => {
    it('should delete an article successfully', async () => {
      const objectId = '611c1393f3d7eb3e14f4e7e0';

      const deleteResult = {
        n: 1,
        ok: 1,
        deletedCount: 1,
      };

      (articleModel.deleteOne as jest.Mock).mockReturnValueOnce({
        exec: jest.fn().mockResolvedValueOnce(deleteResult),
      });

      const result = await articleService.deleteArticle(objectId);

      expect(articleModel.deleteOne).toHaveBeenCalledWith({ _id: objectId });
      expect(result).toEqual(deleteResult);
    });

    it('should throw an error if the delete operation fails', async () => {
      const objectId = '611c1393f3d7eb3e14f4e7e0';
      const error = new Error('Database error');

      (articleModel.deleteOne as jest.Mock).mockReturnValueOnce({
        exec: jest.fn().mockRejectedValueOnce(error),
      });

      await expect(articleService.deleteArticle(objectId)).rejects.toThrow(
        error
      );
    });
  });
});

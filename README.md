# Nest.js - API Server

This project was built using node.js (18.16.0) along with nest.js as a framework, it connects to a Hacker News API to retrieve and save articles about Node.js every hour. It also provides an API to obtain certain data using filters and delete these too.

## Installation
To install this project you need to have [Node.js](https://nodejs.dev/en/download/) installed. Once that is done you need to clone this project by using on a terminal (you also need [Git](https://git-scm.com/downloads) to run this command):

```sh
git clone https://gitlab.com/ArielRx/backend-test.git
```

Now before running the project you need to install the dependencies using:

```sh
npm install
```

After that, you should be able to use the next command to run the project:

```sh
npm run dev
```

The server will start on the default port: `3000`. After this you should be able to access to its API by going to: `http://localhost:3000` 

## Docker usage
The project has a multistage Docker file set up to be able to build an image and run it afterwards on a container.

All you need to do is to have [Docker](https://docs.docker.com/engine/install/ubuntu/) installed and run the next command in the root directory:

```sh
docker build -t {image-name} .
```

Once the image is done you can run the next command to get the server up on a container:

```sh
docker run -p 3000:3000 {image-name}
```

Then after that, you should be able to access it by going to: `http://localhost:3000` 

## Gitlab pipeline

This repository has Gitlab's pipeline runners active to run tests and lints, which were set up on their respective files: `article.service.spec.ts` and `.eslintrc.js` 

To be able to run through these jobs it needs to make push to the `tests` branch as it is the configured branch to run these.


> Note that the keys.ts file in the config folder contains the mongoURI for the database. You can use this URI to retrieve information and manipulate data, including using the delete method mentioned earlier.
export class CreateArticleDto {
  objectID: string;
  title: string;
  author: string;
  tags: [string];
  url: string;
  createdAt: Date;
}

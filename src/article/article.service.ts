import { Injectable } from '@nestjs/common';
import { Article } from '../../src/schemas/article.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import axios from 'axios';
import keys from '../../config/keys';
import { GetArticleDto } from './dto/get-article.dto';

@Injectable()
export class ArticleService {
  constructor(
    @InjectModel(Article.name) private articleModel: Model<Article>
  ) {}

  async fetchAndSaveArticles() {
    // First we attempt to retrieve the data from the API and save it to loop it afterwards.
    let response: any;
    try {
      response = await axios.get(keys.apiURL);
    } catch (error) {
      console.log(error);
    }

    // We store a copy of articles after the loop its done with its data
    const articles = response.data.hits.map((hit) => {
      const { objectID, author, title, url, created_at, _tags } = hit;
      return {
        objectID,
        author,
        title,
        url,
        createdAt: new Date(created_at),
        tags: _tags,
      };
    });

    // Here we attempt to save the articles array to the database
    try {
      await this.articleModel.create(articles);
      console.log('Articles saved by cron job');
    } catch (error) {
      console.log(error);
      throw new Error('Database error');
    }
  }

  async getArticles(getArticleDto: GetArticleDto): Promise<Article[]> {
    const page = 1;
    const limit = 5;
    const { author, tags, title } = getArticleDto;
    // Build up the query with page 1 and limit 5 as requested to later attach its filters if they are provided
    const query = this.articleModel
      .find()
      .sort({ createdAt: -1 })
      .skip((page - 1) * limit)
      .limit(limit);

    if (author) query.where({ author: { $in: author } });
    if (tags) query.where({ tags: { $in: tags } });
    if (title) query.where({ title: { $in: title } });

    let articles: Article[];
    // Attempt to retrieve the data using the query to return it afterwards
    try {
      articles = await query.exec();
    } catch (error) {
      console.log(error);
      throw new Error('Database error');
    }
    return articles;
  }

  async deleteArticle(objectID: string) {
    try {
      return await this.articleModel.deleteOne({ _id: objectID }).exec();
    } catch (error) {
      console.log(error);
      throw new Error('Database error');
    }
  }
}

import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type ArticleDocument = HydratedDocument<Article>;

@Schema()
export class Article {
  @Prop()
  title: string;

  @Prop()
  author: string;

  @Prop()
  tags: [string];

  @Prop()
  url: string;

  @Prop()
  createdAt: Date;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);

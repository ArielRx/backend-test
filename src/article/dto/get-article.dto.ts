import { ApiProperty } from '@nestjs/swagger';

export class GetArticleDto {
  @ApiProperty()
  author?: string;
  @ApiProperty()
  tags?: [string];
  @ApiProperty()
  title?: string;
}
